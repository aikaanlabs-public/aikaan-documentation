DOCKER_IMAGE=aikaan/aikaan-docs:latest

all:docker-build

package-build:
	cd docs && hugo
docker-build:package-build
	docker build -t $(DOCKER_IMAGE) .

docker-publish:docker-build
	docker push $(DOCKER_IMAGE)

clean:
	rm -rf docs/public
