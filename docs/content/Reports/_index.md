+++
title = "Reports"
date = 2020-04-24T14:20:24+05:30
weight = 6
chapter = true
pre = "<b>X. </b>"
description = "Summarized information across your devices"
+++

# Reports

Summarized information across your devices
