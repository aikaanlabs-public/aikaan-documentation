---
title: "Setting Device Names"
date: 2020-04-24T15:48:09+05:30
weight: 4
---
What is device name?
--------------------

Every device on the AiKaan dashboard has two identity. Device ID and
device name. Device ID is system generated, UUID. And device name is
name of the device in human readable format.

Each device once registered, gets a random name human readable name,
similar to docker names. [Someone please update the library used for
generating random name] 

How do we change the device name?
---------------------------------

Device name can be edited on the dashboard. As of writing this document,
any user can change the device name.

Once changed, the device name will be retained, until edited again,
across reboot of device or controller.

 

How do we change the device name automatically ?
------------------------------------------------

Often there is a need to change the default device name, based on
chassis ID or device serial number etc. This can be easily achieved by
adding a file.

### The location of the file

Create file "mender-inventory-\*" on the device at the location
\$AGENT\_INSTALL\_DIR/opt/aikaan/bin/support/inventory/. Replace \* with
any name of your continent, for example 

> /opt/aikaan/bin/support/inventory/mender-inventory-sn2name.sh

The file must have executable permisson

> chmod +x /opt/aikaan/bin/support/inventory/mender-inventory-sn2name.sh

### What should file do

This file, when executed print "device\_name=MyNewSerialNumber" (without
quotes). Consider a raspberry PI, we can get the serial number as below

> bash\# cat /proc/cpuinfo | grep Serial \
> Serial          : 000000000000000d
>
> bash\# serial\_num=cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2
>
> bash\# echo \$serial\_num\
> 000000000000000d

For this device the file mender-inventory-sn2name.sh can look like this

> *\#!/bin/bash*\
> *serial\_num=cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2*\
> *echo "device\_name=\$serial\_num"*


