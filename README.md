# Aikaan Documentation

This Repository has all the necessary files for Aikaan documentation hosted at docs.aikaan.io


# Prerequisites

1.  **Hugo** - Download and install from [HERE](https://gohugo.io/getting-started/installing/)
2.  **Docker**
3.  **docker-compose**

# How to make changes

1.  You need to know Markdown ([THIS](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) might help)
2. Find which markdown file you'd like to edit under docs folder
3. Make a Pull Request. Once accepted , it would be published to docs.aikaan.io


**Note**: Incase you need to create a new chapter , then you need to refer to hugo docs ( [THIS](https://medium.com/@amirm.lavasani/a-fast-way-to-create-docs-using-hugo-framework-e49b7cb582af) is a good intro)
# How to build and run

1. Run **make docker-build** to build hugo and the docker running it
2. Run **docker-compose up -d** to start a docker which renders the UI

**Note** : Please note the port number in docker-compose.yml to know which port the UI is being rendered at

# How to publish to docs.aikaan.io

1. Run **make docker-publish** it would publish the docker to docker hub and should reflect in docs.aikaan.io in a few minutes.


